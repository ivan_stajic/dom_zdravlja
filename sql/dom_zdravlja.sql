DROP SCHEMA IF EXISTS dom_zdravlja;
CREATE SCHEMA dom_zdravlja DEFAULT CHARACTER SET utf8;
USE dom_zdravlja;

CREATE TABLE pacijenti(
	id INT AUTO_INCREMENT,
	LBO VARCHAR(20) NOT NULL,
	ime VARCHAR(20) NOT NULL,
    prezime VARCHAR(20) NOT NULL,
    datumRodjenja DATE NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE tipovipregleda(
	id INT AUTO_INCREMENT,
	naziv VARCHAR(30) NOT NULL,
    cena DOUBLE NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE pregledi(
	id INT AUTO_INCREMENT,
	datumIVreme DATETIME NOT NULL,
    tipPregleda INT NOT NULL,
    pacijent INT NOT NULL,
    placen TINYINT(1) NOT NULL,
	PRIMARY KEY (id),
	
	FOREIGN KEY (tipPregleda) REFERENCES tipovipregleda(id)
	    ON DELETE RESTRICT,
	FOREIGN KEY (pacijent) REFERENCES pacijenti(id)
	    ON DELETE RESTRICT
);

INSERT INTO pacijenti (id, LBO, ime, prezime, datumRodjenja) VALUES (1, 11111111111, 'Petar', 'Petrovic', '2000-02-05');
INSERT INTO pacijenti (id, LBO, ime, prezime, datumRodjenja) VALUES (2, 22222222222, 'Dragan', 'Draganovic', '1999-01-18');
INSERT INTO pacijenti (id, LBO, ime, prezime, datumRodjenja) VALUES (3, 33333333333, 'Marko', 'Markovic', '2001-07-15');
INSERT INTO pacijenti (id, LBO, ime, prezime, datumRodjenja) VALUES (4, 44444444444, 'Ivan', 'Ivanovic', '1988-11-24');
INSERT INTO pacijenti (id, LBO, ime, prezime, datumRodjenja) VALUES (5, 55555555555, 'Sava', 'Savic', '1986-05-05');

INSERT INTO tipovipregleda (id, naziv, cena) VALUES (1, 'opsti', 2000.00);
INSERT INTO tipovipregleda (id, naziv, cena) VALUES (2, 'specijalisticki', 3000.00);
INSERT INTO tipovipregleda (id, naziv, cena) VALUES (3, 'dijagnosticki', 10000.00);
INSERT INTO tipovipregleda (id, naziv, cena) VALUES (4, 'laboratorijski', 500.00);

INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (1, '2017-08-08 12:00:00', 2, 5, 0);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (2, '2016-07-12 11:00:00', 3, 4, 1);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (3, '2015-01-21 10:00:00', 3, 3, 0);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (4, '2017-03-24 07:00:00', 4, 2, 1);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (5, '2012-04-02 02:00:00', 1, 1, 0);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (6, '2014-10-14 01:00:00', 3, 1, 0);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (7, '2011-08-17 11:00:00', 1, 2, 1);
INSERT INTO pregledi (id, datumIVreme, tipPregleda, pacijent, placen) VALUES (8, '2000-12-09 12:00:00', 1, 1, 1);
