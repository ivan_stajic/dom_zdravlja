package club.ivanstajic.dom_zdravlja;

public class TipPregleda {
	
	protected int id;
	protected String nazivTipa;
	protected double cena;
	
	public TipPregleda(int id, String nazivTipa, double cena) {
		super();
		this.id = id;
		this.nazivTipa = nazivTipa;
		this.cena = cena;
	}

	@Override
	public String toString() {
		return "TipPregleda [id=" + id + ", nazivTipa=" + nazivTipa + ", cena=" + cena + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipPregleda other = (TipPregleda) obj;
		if (Double.doubleToLongBits(cena) != Double.doubleToLongBits(other.cena))
			return false;
		if (id != other.id)
			return false;
		if (nazivTipa == null) {
			if (other.nazivTipa != null)
				return false;
		} else if (!nazivTipa.equals(other.nazivTipa))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNazivTipa() {
		return nazivTipa;
	}

	public void setNazivTipa(String nazivTipa) {
		this.nazivTipa = nazivTipa;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}
	

}
