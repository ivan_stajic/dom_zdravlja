package club.ivanstajic.dom_zdravljaUTILS;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import club.ivanstajic.dom_zdravlja.Pacijent;
import club.ivanstajic.dom_zdravljaDAO.PacijentDAO;
import club.ivanstajic.dom_zdravljaUI.ApplicationUI;

public class PomocnaKlasa {

	static Scanner sc = new Scanner(System.in);
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

	// citanje promenljive Date
	public static Date ocitajDatum() {
		String tekst = "";
		boolean formatirano = false;
		Date datum = null;
		while (!formatirano || tekst.equals("") || tekst == null) {
			System.out.println("Unesite vremenski period u formatu yyyy-MM-dd");
			tekst = sc.nextLine();
			try {
				datum = sdf2.parse(tekst);
				formatirano = true;
			} catch (ParseException pe) {
				System.out.println("Niste uneli podatak validno. Pokusajte ponovo.");
			}
		}
		return datum;
	}
	
	public static boolean proveriDatum(Date datum) {
		String tekst = "";
		boolean formatirano = false;
		datum = ocitajDatum();
		Date datumTrenutni = new Date();
		while (!formatirano ||datum.before(datumTrenutni) || datum.equals(datumTrenutni)) {
			//tekst = sc.nextLine();
			try {
				datum = sdf2.parse(tekst);
				formatirano = true;
			} catch (ParseException pe) {
				System.out.println("Datum rodjenja mora biti manji od trenutnog datuma. Pokusajte ponovo.");
			}
		}
		return formatirano;
	}
	
	// provera promenljive LBO
	public static boolean isLBOValid(String lbo) {
		if (lbo.equals("")) {
			return false;
		}
		
		boolean containsDigitsOnly = true;
		for (int i = 0; i < lbo.length(); i++) {
			if (!Character.isDigit(lbo.charAt(i))) {
				containsDigitsOnly = false;
				break;
			}
		}
		return lbo.length() == 11 && containsDigitsOnly;
	}
	
	public static boolean ifLBOExists(String lbo) {
		if (lbo.equals("")) {
			return false;
		}
		List<Pacijent> pacijenti = PacijentDAO.getAll(ApplicationUI.getConn());
		boolean exists = false;
		for (int i = 0; i < pacijenti.size(); i++) {
			if (lbo.equals(pacijenti.get(i).getLBO())) {
				exists = true;
			}
		}
		return exists;
	}


	// citanje promenljive String
	public static String ocitajTekst() {
		String tekst = "";
		while (tekst == null || tekst.equals(""))
			tekst = sc.nextLine();

		return tekst;
	}

	// citanje promenljive integer
	public static int ocitajCeoBroj() {
		while (sc.hasNextInt() == false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		int ceoBroj = sc.nextInt();
		sc.nextLine(); // cisti sve sa ulaza sto nije broj ili ostatak teste
						// posla broja
		return ceoBroj;
	}

	// citanje promenljive double
	public static float ocitajRealanBroj() {

		while (sc.hasNextFloat() == false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			sc.nextLine();
		}
		float realanBroj = sc.nextFloat();
		sc.nextLine(); // cisti sve sa ulaza sto nije broj ili ostatak teste
						// posla broja
		return realanBroj;
	}

	// citanje promenljive char
	public static char ocitajKarakter() {

		String text;
		while ((text = sc.next()) == null || text.length() != 1) {
			System.out.println("GRESKA - Pogresno unsesena vrednost za karakter, pokusajte ponovo: ");
			sc.nextLine();
		}
		char karakter = text.charAt(0);
		return karakter;
	}

	// citanje promenljive char
	public static char ocitajOdlukuOPotvrdi(String tekst) {
		System.out.println("Da li zelite " + tekst + " [Y/N]:");
		char odluka = ' ';
		while (!(odluka == 'Y' || odluka == 'N')) {
			odluka = ocitajKarakter();
			if (!(odluka == 'Y' || odluka == 'N')) {
				System.out.println("Opcije su Y ili N");
			}
		}
		return odluka;
	}
	
	static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	static boolean isBoolean(String s) {
		try {
			Boolean.parseBoolean(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	

}
