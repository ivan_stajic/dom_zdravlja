package club.ivanstajic.dom_zdravlja;

import java.util.Date;

import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class Pregled {
	
	protected int id;
	protected Date datumIVreme;
	protected TipPregleda tipPregleda;
	protected Pacijent pacijent;
	protected int placen;
	
	
	
	public Pregled(Date datumIVreme, TipPregleda tipPregleda, Pacijent pacijent, int placen) {
		super();
		this.datumIVreme = datumIVreme;
		this.tipPregleda = tipPregleda;
		this.pacijent = pacijent;
		this.placen = placen;
	}

	public Pregled(int id, Date datumIVreme, TipPregleda tipPregleda, Pacijent pacijent, int placen) {
		super();
		this.id = id;
		this.datumIVreme = datumIVreme;
		this.tipPregleda = tipPregleda;
		this.pacijent = pacijent;
		this.placen = placen;
	}

	@Override
	public String toString() {
		return "Pregled [id=" + id + ", datumIVreme=" + datumIVreme + ", tipPregleda=" + tipPregleda + ", pacijent="
				+ pacijent + ", placen=" + placen + "]";
	}
	
	public String toFormatedString() {
		return String.format("%-22s %-20s %10.2f %9s\n", PomocnaKlasa.sdf.format(datumIVreme), tipPregleda.getNazivTipa(), tipPregleda.getCena(), placen);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pregled other = (Pregled) obj;
		if (datumIVreme == null) {
			if (other.datumIVreme != null)
				return false;
		} else if (!datumIVreme.equals(other.datumIVreme))
			return false;
		if (id != other.id)
			return false;
		if (pacijent == null) {
			if (other.pacijent != null)
				return false;
		} else if (!pacijent.equals(other.pacijent))
			return false;
		if (placen != other.placen)
			return false;
		if (tipPregleda == null) {
			if (other.tipPregleda != null)
				return false;
		} else if (!tipPregleda.equals(other.tipPregleda))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatumIVreme() {
		return datumIVreme;
	}

	public void setDatumIVreme(Date datumIVreme) {
		this.datumIVreme = datumIVreme;
	}

	public TipPregleda getTipPregleda() {
		return tipPregleda;
	}

	public void setTipPregleda(TipPregleda tipPregleda) {
		this.tipPregleda = tipPregleda;
	}

	public Pacijent getPacijent() {
		return pacijent;
	}

	public void setPacijent(Pacijent pacijent) {
		this.pacijent = pacijent;
	}

	public int getPlacen() {
		return placen;
	}

	public void setPlacen(int placen) {
		this.placen = placen;
	}
	
	

}
