package club.ivanstajic.dom_zdravljaDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import club.ivanstajic.dom_zdravlja.Pacijent;
import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class PacijentDAO {
	
	public static List<Pacijent> getAll(Connection conn){
		List<Pacijent> pacijenti = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT * FROM pacijenti";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String lbo = rset.getString(index++);
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				Date datumRodjenja = PomocnaKlasa.sdf2.parse(rset.getString(index++));
				
				Pacijent pacijent = new Pacijent(id, lbo, ime, prezime, datumRodjenja);
				pacijenti.add(pacijent);
			}
			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pacijenti;
	}
	
	public static Pacijent getByID(Connection conn, int id) {
		Pacijent pacijent = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM pacijenti WHERE id = " + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int idP = rset.getInt(index++);
				String lbo = rset.getString(index++);
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				Date datumRodjenja = null;
				try {
					datumRodjenja = PomocnaKlasa.sdf2.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				pacijent = new Pacijent(idP, lbo, ime, prezime, datumRodjenja);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return pacijent;
	}
	
	public static boolean add(Connection conn, Pacijent pacijent) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO pacijenti (LBO, ime, prezime, datumRodjenja) VALUES (?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, pacijent.getLBO());
			pstmt.setString(index++, pacijent.getIme());
			pstmt.setString(index++, pacijent.getPrezime());
			pstmt.setString(index++, PomocnaKlasa.sdf.format(pacijent.getDatumRodjenja()));

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}




}
