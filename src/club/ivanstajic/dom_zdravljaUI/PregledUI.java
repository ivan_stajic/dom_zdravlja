package club.ivanstajic.dom_zdravljaUI;

import java.util.Date;

import club.ivanstajic.dom_zdravlja.Pacijent;
import club.ivanstajic.dom_zdravlja.Pregled;
import club.ivanstajic.dom_zdravlja.TipPregleda;
import club.ivanstajic.dom_zdravljaDAO.PacijentDAO;
import club.ivanstajic.dom_zdravljaDAO.PregledDAO;
import club.ivanstajic.dom_zdravljaDAO.TipPregledaDAO;
import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class PregledUI {
	
	public static void unosNovogPregleda() {
		
		System.out.print("Unesi ID pacijenta:");
		int idP = PomocnaKlasa.ocitajCeoBroj();
		Pacijent pacijent = PacijentDAO.getByID(ApplicationUI.getConn(), idP);
		while (pacijent == null) {
			System.out.println("Pacijent sa ID-jem " + idP
					+ " vec postoji u evidenciji");
			idP = PomocnaKlasa.ocitajCeoBroj();
		}
		
		System.out.print("Unesi ID tipa pregleda:");
		int idTP = PomocnaKlasa.ocitajCeoBroj();
		TipPregleda tipPregleda = TipPregledaDAO.getByID(ApplicationUI.getConn(), idTP);
		while (tipPregleda == null) {
			System.out.println("Tip pregleda sa ID-jem " + idTP
					+ " vec postoji u evidenciji");
			idTP = PomocnaKlasa.ocitajCeoBroj();
		}
		
		Date datumDanasnji = new Date();
		//String datum = PomocnaKlasa.sdf.parse(datumDanasnji);
		
		int placen = 0;
		
		Pregled pr = new Pregled(0, datumDanasnji,TipPregledaDAO.getByID(ApplicationUI.getConn(), idTP), PacijentDAO.getByID(ApplicationUI.getConn(), idP), placen);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		PregledDAO.add(ApplicationUI.getConn(), pr);
	}
	
}
