package club.ivanstajic.dom_zdravljaUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class ApplicationUI {
	
private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/dom_zdravlja?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}


	public static void main(String[] args) {

		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa.");
				break;
			case 1:
				PacijentUI.unosNovogPacijenta();
				break;
			case 2:
				PregledUI.unosNovogPregleda();
				break;
			case 3:
				PacijentUI.ispisiPacijenteSaPregledima();
				break;
//			case 4:
//				MonifestacijaUI.izmeniNazivManifestacije();
//				break;
//			case 5:
//				MonifestacijaUI.izmeniNazivManifestacije();
//				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
		
		
		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Kulturne Manifestacije - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Dodavanje pacijenta");
		System.out.println("\tOpcija broj 2 - Dodavanje pregleda");
		System.out.println("\tOpcija broj 3 - Prikaz svih pacijenata sa pregledima");
		System.out.println("\tOpcija broj 4 - Placanje");
		System.out.println("\tOpcija broj 5 - Izvestaj po tipu pregleda");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}

}
