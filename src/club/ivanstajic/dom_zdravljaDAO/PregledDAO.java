package club.ivanstajic.dom_zdravljaDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import club.ivanstajic.dom_zdravlja.Pregled;
import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class PregledDAO {
	
	public static List<Pregled> getAll(Connection conn){
		List<Pregled> pregledi = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT datumIVreme, tipPregleda, pacijent, placen FROM pregledi";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				Date datumIVreme = PomocnaKlasa.sdf.parse(rset.getString(index++));
				int tipPregleda = rset.getInt(index++);
				int pacijent = rset.getInt(index++);
				int placen = rset.getInt(index++);
				
				
				Pregled pregled = new Pregled(datumIVreme, TipPregledaDAO.getByID(conn, tipPregleda), PacijentDAO.getByID(conn, pacijent), placen);
				pregledi.add(pregled);
			}
			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pregledi;
	}
	
	public static List<Pregled> getPreglediByPacijentId(Connection conn, int id){
		List<Pregled> pregledi = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT datumIVreme, tipPregleda, pacijent, placen FROM pregledi WHERE pacijent = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				Date datumIVreme = PomocnaKlasa.sdf.parse(rset.getString(index++));
				int tipPregleda = rset.getInt(index++);
				int pacijent = rset.getInt(index++);
				int placen = rset.getInt(index++);
				
				
				Pregled pregled = new Pregled(id, datumIVreme, TipPregledaDAO.getByID(conn, tipPregleda), PacijentDAO.getByID(conn, pacijent), placen);
				pregledi.add(pregled);
			}
			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pregledi;
	}
	
	public static boolean add(Connection conn, Pregled pregled) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO pregledi (datumIVreme, tipPregleda, pacijent, placen) VALUES (?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, PomocnaKlasa.sdf.format(pregled.getDatumIVreme()));
			pstmt.setInt(index++, pregled.getTipPregleda().getId());
			pstmt.setInt(index++, pregled.getPacijent().getId());
			pstmt.setInt(index++, pregled.getPlacen());
			

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	
	


}
