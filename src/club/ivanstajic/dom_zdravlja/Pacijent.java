package club.ivanstajic.dom_zdravlja;

import java.util.ArrayList;
import java.util.Date;

import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class Pacijent {
	
	protected int id;
	protected String LBO;
	protected String ime;
	protected String prezime;
	protected Date datumRodjenja;
	ArrayList<Pregled> sviPregledi = new ArrayList<Pregled>();
	
	public Pacijent(int id, String lBO, String ime, String prezime, Date datumRodjenja) {
		super();
		this.id = id;
		LBO = lBO;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
	}

	@Override
	public String toString() {
		return "Pacijent [id=" + id + ", LBO=" + LBO + ", ime=" + ime + ", prezime=" + prezime + ", datumRodjenja="
				+ datumRodjenja + ", sviPregledi=" + sviPregledi + "]";
	}

	public String toFormatedString() {
		return String.format("%-4s %-15s %-15s %-15s %12s\n", id, LBO, ime, prezime, PomocnaKlasa.sdf2.format(datumRodjenja));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pacijent other = (Pacijent) obj;
		if (LBO == null) {
			if (other.LBO != null)
				return false;
		} else if (!LBO.equals(other.LBO))
			return false;
		if (datumRodjenja == null) {
			if (other.datumRodjenja != null)
				return false;
		} else if (!datumRodjenja.equals(other.datumRodjenja))
			return false;
		if (id != other.id)
			return false;
		if (ime == null) {
			if (other.ime != null)
				return false;
		} else if (!ime.equals(other.ime))
			return false;
		if (prezime == null) {
			if (other.prezime != null)
				return false;
		} else if (!prezime.equals(other.prezime))
			return false;
		if (sviPregledi == null) {
			if (other.sviPregledi != null)
				return false;
		} else if (!sviPregledi.equals(other.sviPregledi))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLBO() {
		return LBO;
	}

	public void setLBO(String lBO) {
		LBO = lBO;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Date getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public ArrayList<Pregled> getSviPregledi() {
		return sviPregledi;
	}

	public void setSviPregledi(ArrayList<Pregled> sviPregledi) {
		this.sviPregledi = sviPregledi;
	}

}
