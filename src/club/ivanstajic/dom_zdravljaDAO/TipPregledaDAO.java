package club.ivanstajic.dom_zdravljaDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import club.ivanstajic.dom_zdravlja.TipPregleda;


public class TipPregledaDAO {
	
	public static TipPregleda getByID(Connection conn, int id) {
		TipPregleda tip = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM tipovipregleda WHERE id = " + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int idP = rset.getInt(index++);
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				
				tip = new TipPregleda(idP, naziv, cena);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return tip;
	}


}
