package club.ivanstajic.dom_zdravljaUI;

import java.util.Date;
import java.util.List;

import club.ivanstajic.dom_zdravlja.Pacijent;
import club.ivanstajic.dom_zdravlja.Pregled;
import club.ivanstajic.dom_zdravljaDAO.PacijentDAO;
import club.ivanstajic.dom_zdravljaDAO.PregledDAO;
import club.ivanstajic.dom_zdravljaUTILS.PomocnaKlasa;

public class PacijentUI {
	
	public static Pacijent pronadjiPacijentaPoId() {
		Pacijent rez = null;
		
		while (rez == null) {
			System.out.println("Unesite id pacijenta: ");
			int trazeniID = PomocnaKlasa.ocitajCeoBroj();
			rez = PacijentDAO.getByID(ApplicationUI.getConn(), trazeniID);
		}
		return rez;
	}

	
	public static void ispisiPacijenteSaPregledima() {
		List<Pacijent> pacijenti = PacijentDAO.getAll(ApplicationUI.getConn());
		List<Pregled> pregledi = PregledDAO.getAll(ApplicationUI.getConn());
		
		for (Pacijent pac : pacijenti) {
			System.out.println("\n=================================================================");
			System.out.printf("%-4s %-15s %-15s %-15s %12s\n", "ID", "LBO", "IME", "PREZIME", "DA.RODJ.");
			System.out.println("=================================================================");
			System.out.print(pac.toFormatedString());
			System.out.println("\n\t\t\tPREGLEDI:");
			System.out.println("-----------------------------------------------------------------");
			System.out.printf("%-22s %-20s %10s %9s\n", "Datum", "Tip", "Cena", "Placen");
			System.out.println("-----------------------------------------------------------------");
			for(Pregled pr : pregledi) {
				if(pac.getId()==pr.getPacijent().getId()) {
				System.out.print(pr.toFormatedString());
				}
			}
			System.out.println("=================================================================");

		}
	}
	
	public static void unosNovogPacijenta() {
		
		System.out.print("Unesi LBO (11 karaktera):");
		String lbo = PomocnaKlasa.ocitajTekst();
		
		while (!(PomocnaKlasa.ifLBOExists(lbo) || PomocnaKlasa.isLBOValid(lbo))) {
			System.out.println("LBO " + lbo
					+ " nije validan, ili vec postoji u evidenciji");
			lbo = PomocnaKlasa.ocitajTekst();
		}
		
		System.out.print("Unesi ime pacijenta:");
		String ime = PomocnaKlasa.ocitajTekst();
		
		System.out.print("Unesi prezime pacijenta:");
		String prezime = PomocnaKlasa.ocitajTekst();
		
		System.out.println("Unesi datum rodjenja");
		Date datumRodjenja = PomocnaKlasa.ocitajDatum();
		Date datumDanasnji = new Date();
		
		while(datumDanasnji.before(datumRodjenja)){
			System.out.println("Datum rodjenja mora biti pre trenutnog datuma.");
			datumRodjenja = PomocnaKlasa.ocitajDatum();
		}
		
		Pacijent pa = new Pacijent(0, lbo, ime, prezime, datumRodjenja);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		PacijentDAO.add(ApplicationUI.getConn(), pa);
	}

	


}
